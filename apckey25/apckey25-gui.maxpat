{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 62.0, 79.0, 828.0, 683.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 171.0, 500.0, 101.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 470.0, 108.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_subgrid"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 440.0, 101.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_scene"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 410.0, 105.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_names"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 380.0, 99.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_menu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 350.0, 101.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_knobs"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 320.0, 93.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_keys"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 287.0, 95.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_grid2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 171.0, 257.0, 89.0, 22.0 ],
					"style" : "",
					"text" : "apckey25_grid"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "apckey25_grid.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ -2.0, 0.0, 242.0, 150.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "apckey25_grid2.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 429.0, 257.0, 357.0, 156.0 ],
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [  ],
		"dependency_cache" : [ 			{
				"name" : "apckey25_grid2.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_toggle.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "divmod.maxpat",
				"bootpath" : "./Max 6.1/packages/RTC-lib_70/patchers/Toolbox",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_grid.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "midi-device.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/_artefacts/midi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_keys.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "moses.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/_artefacts",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_knobs.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_menu.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_names.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_scene.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "apckey25_subgrid.maxpat",
				"bootpath" : "~/Google Drive/OFFLINE/MaxMSP ~PATCHES/APCkey25",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
